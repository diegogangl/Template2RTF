<?php
/**
 * Template2RTF - Create a RTF file from a template and a set of variables
 * Copyright (C) 2009 - Diego N. Garcia Gangl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Namespace. Only works with php >5.3
 */
//namespace utils\rtf;

/**
 * Template2RTF
 *
 *  Class to fill RTF templates with variables.
 *  Variables must be in an associative array
 *  and the keys must match the placeholder
 *  keywords (without the tag).
 *
 *  You also have to use a special "tag" in the
 *  template and specify it here. (default is
 *  "%%"). Example: %%firstname%%.
 *
 *  You can make the RTF directly from the
 *  constructor by passing all variables.
 *  Or you can do it "the fluent way" by only
 *  passing the template filename and then
 *  using the setter methods.
 *
 *  Examples:
 *  <code>
 *  $rtf = new Template2RTF('myTemplate.rtf', $vars, 'new.rtf');
 *
 *  $rtf = new Template2RTF('myTemplate.rtf');
 *  $rtf->withVars($vars)
 *      ->toFile('new.rtf')
 *      ->write();
 *  </code>
 *
 * Populate code taken from:
 *  http://php5unleashed.atw.hu/ch28lev1sec1.html
 *
 * @version     1.5
 * @author      Diego Garcia Gangl <dnicolas@gmail.com>
 * @package     utils
 * @copyright   2009 - Diego Garcia Gangl
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 */
class Template2RTF
{
    /**
     * The final RTF filled with $vars
     * 
     * @var     string
     * @see     $vars
     * @access  protected
     */
    protected   $finalRTF       = null;

   /**
    * Template to use
    *
    * @var     string
    * @access  protected
    */
    protected   $template       =  null;


   /**
    * The filename to write to
    *
    * @var     string
    * @see     toFile()
    * @access  protected
    */
    protected   $filename       = null;

   /**
    * The tag to find keywords to match.
    * (this must surround every keyword).
    *
    * @var     string
    * @see     populate()
    * @access  protected
    */
    protected   $tag            = '%%';


   /**
    * Array containing the variables to replace
    * the keywords with.
    *
    * @var     array
    * @see     populate()
    * @access  protected
    */
    protected   $vars           =  array();

   /**
    * Array 
    *
    * @var     array
    * @see     populate()
    * @access  protected
    */
    protected   $replacements   =   array(  '\\' => "\\\\",
                                            '{'  => "\{",
                                            '}'  => "\}"
                                         );
    
    /**
     * Constructor
     * 
     * @param   string    $template  Template to use
     * @param   array     $vars      Data to use
     * @param   string    $toFile    Where to write the resulting rtf
     * @param   string    $tag       Tags used in the template
     * @access  public
     */
    public function __construct($template, array $vars = null, $toFile = null, $tag = '%%')
    {
        $this->template    =   $template;
        $this->tag         =   $tag;

        // If filename is null, we default to the template filename
        $this->filename    =   ($toFile) ? $toFile : $template;

        if ( is_array($vars) )
        {
           $this->vars  =   $vars;
           $this->make($this->filename);
        }

        return $this;
    }

    /**
     * Sets the Tag to use to find keywords.
     *
     * @param   string  $tag   Tag
     * @access  public
     */
    public function usingTag($tag)
    {
        $this->tag =   $tag;
        return $this;
    }

    /**
     * Sets the template to use.
     *
     * @param   string  $template   Template to use
     * @access  public
     */
    public function usingTemplate($template)
    {
        $this->template =   $template;
        return $this;
    }

    /**
     * Sets new filename to write the final RTF.
     *
     * @param   string  $file   Filename
     * @access  public
     */
    public function toFile($file)
    {
        $this->filename    =   $file;
        return $this;
    }

    /**
     * Sets the variables array.
     *
     * @param   array   $vars   Variables to use
     * @access  public
     */
    public function withVars(array $vars)
    {
        $this->vars  =  $vars;
        return $this;
    }


    /**
     * Puts the generated RTF file as a string
     * in the given variable.
     *
     * @param   string   $var   Variable to hold the final rtf
     * @access  public
     */
     public function putIn(&$var)
     {
        $this->finalRTF = $this->populate();

        $var = $this->finalRTF;
        return $this;
     }


     /**
      * Writes the final RTF to a file
      *
      * @access  public
      */
     public function write()
     {
        $this->finalRTF = $this->populate();

        $fileStream = fopen($this->filename, 'w') ;
        fwrite($fileStream, $this->finalRTF);
        fclose($fileStream);

        return $this;
     }


    /**
     * Creates headers to send the RTF to
     * a browser.
     *
     * @access public
     */
    public function headers()
    {
        header("Content-type: application/msword");
        header("Content-disposition: inline; filename=". $this->filename);
        header("Content-length: " . strlen($this->finalRTF));

        return $this;
    }


    /**
     * Populates the RTF template with the variables contained
     * in $vars and returns it.
     *
     * @link    http://php5unleashed.atw.hu/ch28lev1sec1.html
     * @access  private
     * @return  string
     */
    protected function populate()
    {

        $template = file_get_contents($this->template);

        if(!$template)
        {
            return false;
        }

        // Loop through the vars finding matches in the template
        foreach($this->vars as $key => $value)
        {
            $search = $this->tag . $key . $this->tag;

            foreach($this->replacements as $orig => $replace)
            {
                $value  = str_replace($orig, $replace, $value);
            }

            // Fix for UTF-8
            $value      = self::unicodeToEntitiesPreservingAscii(self::utf8ToUnicode($value));;
            $template   = str_replace($search, $value, $template);
        }

        return $template;
    }


    /**
     * Converts a string to a list of unicode values.
     *
     * @param   string  $str    String to convert
     * @link    http://www.randomchaos.com/documents/?source=php_and_unicode
     * @access  protected
     * @return  string
     * @static
     */
    protected static function utf8ToUnicode($str)
    {
        $unicode = array();
        $values = array();
        $lookingFor = 1;

        for ($i = 0; $i < strlen($str); $i++ )
        {
            $thisValue = ord($str[$i]);

            if ($thisValue < 128)
            {
                  $unicode[] = $thisValue;
            }
            else
            {
            if ( count( $values ) == 0 )
            {
                 $lookingFor = ( $thisValue < 224 ) ? 2 : 3;
            }

            $values[] = $thisValue;

              if ( count( $values ) == $lookingFor )
              {
                  $number = ( $lookingFor == 3 ) ?
                     ( ( $values[0] % 16 ) * 4096 ) + ( ( $values[1] % 64 ) * 64 ) + ( $values[2] % 64 ):
                        ( ( $values[0] % 32 ) * 64 ) + ( $values[1] % 64 );

                  $unicode[] = $number;
                  $values = array();
                  $lookingFor = 1;
               }
            }
        }

        return $unicode;
    }

    /**
     * Converts characters that fall in the ascii range back from
     * unicode entities.
     *
     * @param   string  Unicode entities
     * @access  protected
     * @link    http://www.randomchaos.com/documents/?source=php_and_unicode
     * @return  string
     * @static
     */
    protected static function unicodeToEntitiesPreservingAscii($unicode)
    {
        $entities = '';

        foreach( $unicode as $value )
        {
            if ($value != 65279)
            {
               $entities .= ( $value > 127 ) ? '\uc0\u' . $value . ' ' : chr( $value );
            }
        }

        return $entities;
    }
}
?>

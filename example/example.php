<?php
    if($_POST)
    {
       // Load the class
       require_once('../rtf2template.inc.php');

       // Make a new instance with the example template
       $rtf = new Template2RTF('template.rtf');

       // If you're using php 5.3
       //$rtf = new \utils\rtf\RTF2Template('template.rtf');

       $rtf->withVars($_POST)           // $_POST contains a key 'name' 
           ->toFile('output.rtf')       // Place it in a file called output.rtf
           ->write();                   // Write the file
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Example for the Template2RTF class</title>
	<meta name="generator" content="Netbeans 6.8">
	<meta name="author" content="Diego Garcia">
</head>
<body>
    <h1>Template2RTF example</h1>
    <p>Write your name below and click generate to generate a file based on template.rtf with your name on it!</p>
    <p style="color: green;"><?php if($_POST){ echo 'Check the file output.rtf in this folder'; }; ?></p>
    <form method="post" action="example.php">
        <label for="name">Call me:</label>
        <input type="text" name="name">
        <button type="submit">Generate</button>
    </form>
</body>
</html>